(ns prueba.core)

;;Función <
(defn función-<-1
  [numero]
  (< numero numero))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 4)
(función-<-2 10 5)
(función-<-3 5 10 15)

;;Función <=
(defn función-<=-1
  [a b]
  (<= a b))

(defn función-<=-2
  [a b c]
  (<= a b c))

(defn función-<=-3
  [a b c d]
  (<= a b c d))

(función-<=-1 10 20)
(función-<=-2 10 5 20)
(función-<=-3 10 30 25 10)

;;Función ==
(defn función-==-1
  [a b]
  (== a b))

(defn función-==-2
  [a b c]
  (== a b c))

(defn función-==-3
  [a b c d]
  (== a b c d))

(función-==-1 10 10)
(función-==-2 10 20 10)
(función-==-3 10 20 10 20)

;;Función >
(defn función->-1
  [a b]
  (> a b))

(defn función->-2
  [a b c]
  (> a b c))

(defn función->3
  [a b c d]
  (> a b c d))

(función->-1 100 200)
(función->-2 100 99 150)
(función->3 99 99 10 100)

;;Función >=
(defn función->=-1
  [a b]
  (>= a b))

(defn función->=-2
  [a b c d e]
  (>= a b c d e))

(defn función->=-3
  [a b d]
  (>= b a d))

(función->=-1 30 20)
(función->=-2 30 20 40 50 60)
(función->=-3 20 20 20)

;;Función Assoc
(defn función-Asoc-1
  [asociado]
  (assoc [10 20 30 ] 0 asociado))

(defn función-Asoc-2
  [asociado]
  (assoc [10 20 30] 1 asociado))

(defn función-Asoc-3
  [asociado]
  (assoc [10 20 30] 2 asociado))

(función-Asoc-1 5)
(función-Asoc-2 100)
(función-Asoc-3 1)

;Función con Concat
(defn función-concat-1
  [a b c]
  concat [a b c])

(defn función-concat-2
  [a b c d]
  (concat [a d a] [a b c]))

(defn función-concat-3
  [[a b c d e i j]]
  (concat [[a b j] [c d e j]]))

(función-concat-1 10 20 30)
(función-concat-2 4 5 6 9)
(función-concat-3 [40 50 60 70 80 90 100])

;;Función Conj
(defn función-Conj-1
  [valor]
  (conj (range valor) valor))

(defn función-Conj-2
  [valor b]
  (conj b valor))

(defn función-Conj-3
  [valor a]
  (conj a valor))

(función-Conj-1 5)
(función-Conj-2 4 [["a" "b" "c"] [1 2 3]])
(función-Conj-3 4 ["a" "b" "c"])

;Función con Cons
(defn función-cons-1
  [valor a  b c d e f]
  (cons valor '(a b c d e f)))

(defn función-cons-2
  [[valor] a b c]
  (cons [valor] [a b c]))

(defn función-cons-3
  [valor b]
 ( cons valor b))

(función-cons-1 10 2 5 6 7 8 7)
(función-cons-2 [1 2] 1 2 3)
(función-cons-3 "a" ["a" "b" "c" "d"])

;Función con Contains
(defn función-contains-1
  [valor a]
  (contains? a  valor))

(defn función-contains-2
  [valor a]
  (contains? a valor))

(defn función-contains-3
  [a b]
  (contains? a b))

(función-contains-1 10 [1 2 3 4 5 6 7])
(función-contains-2 :c [:a :b :c])
(función-contains-3 ["a" "b" "c" "e" "f" "g"] "f")

;Función Count
(defn función-count-1
  [cadena]
  (count cadena))

(defn función-count-2
  [[valor]]
  (count [valor]))

(defn función-coun-3
  [[valores]]
  (count [valores]))

(función-count-1 "saul")
(función-count-2 [1 2 3])
(función-coun-3 [1 "f" true])

;Función  disj
(defn función-disj-1
  [numero a]
  (disj a numero))

(defn función-disj-2
  [a b c]
  (disj c a b))
(defn función-disj-3
  [x a]
  (disj a x))

(función-disj-1 3 #{1 2 3 4 5 6 7 8})
(función-disj-2 20 60 #{10 20 30 40 50 60})
(función-disj-3 true #{"a" true "f" false 20 30 40 10})

;Función Dissoc
(defn función-dissoc-1
  [valor a]
  (dissoc a valor))

(defn función-dissoc-2
  [a b c z]
  (dissoc z a b c ))

(función-dissoc-1 :b {:a 10 :b 20 :c 30 :d 40})
(función-dissoc-2 :e :a 30 {:a true :b 20 :c 30 :d 40 :e false :f 50})

;Función distinct
(defn función-distict-1
  [valores]
  (distinct valores))

(defn función-distinct-2
  [a]
  (distinct a))

(defn función-distinct-3
  [b]
  (distinct b))

(función-distict-1 [2 1 3 1 4 1 5 6 5])
(función-distinct-2 ["a" "b" "c" "a" "d" "e" "f" "a" "d"])
(función-distinct-3 ["b" "c" "a" \c \a true false true false "a" \a])

;Función  distinct?
(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [f]
  (distinct? f))

(defn función-distinct?-3
  [a]
  (distinct? a))

(función-distinct?-1 1 2)
(función-distinct?-2 [1 2 3 3])
(función-distinct?-3 [1 4 55 "a" 66 "d" 2 4 1])

;Función drop-last
(defn función-drop-last-1
  [n]
  (drop-last n))

(defn función-drop-last-2
  [cad]
  (drop-last cad))

(defn función-drop-last-3
  [a b]
  (drop-last b a))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 [1 2 3 4 5 6 7 8 9 10 11])
(función-drop-last-3 [:a 1 :b 2 :c 3 :d 4 :e 5 :f 6] 4)

;Función Empty
(defn función-Empty-1
  [a b c d e f]
  (empty '(a b c d e f)))

(defn función-Empty-2
  [a]
  (empty #{a}))

(defn función-Empty-3
  [pl]
  (empty [pl]))

(función-Empty-1 10 20 30 40 50 60)
(función-Empty-2 ["a" "b" "c" "d" "e" "f"])
(función-Empty-3 [false 1 "a" 3 "b" "c" 5 6 true])

;Función Empty?
(defn función-Empty?-1
  [a]
  (empty? a))

(defn función-Empty?-2
  [a]
  (empty? [a]))

(defn función-Empty?-3
  [ve]
  (empty? [ve]))

(función-Empty?-1 "")
(función-Empty?-2 [1 2 3 4])
(función-Empty?-3 [])

;Función event?
(defn función-event?-1
  [numero]
  (even? numero))

(defn función-event?-2
  [a b]
  (even? (- a b)))

(defn función-event?-3
  [a]
  (even? (first a)))

(función-event?-1 9)
(función-event?-2 9 3)
(función-event?-3 [10 30 40])

;Función con first
(defn función-first-1
  [a]
  (first a))

(función-first-1 [10 20 30 40 50])
(función-first-1 [:alfa :bravo :Gama :Beta])
(función-first-1 #{:a 1 :b 2 :c 30 :d 50})

;Función con falso?
(defn función-falso?-1
 [x]
 (false? x))

(defn función-falso?-2
  [x]
  (false? x))

(defn función-falso?-3
  [x]
  (false? x))

(función-falso?-1 1)
(función-falso?-2 true)
(función-falso?-1 false)

;Función flantten
(defn función-flatten-1
  [a b]
  (flatten [a [b]]))

(defn función-flatten-2
  [a b]
  (flatten [[b][a]]))

(defn función-flatten-3
  [a b c]
  (flatten [a b c]))

(función-flatten-1 [10 20 30] 12)
(función-flatten-2 [2 3 4] [5 9 5])
(función-flatten-3 [false 1 5 "f" \a] [true false 1 2] [12 13])

;Función frecuencias
(defn función-frequencies-1
  [a]
  (frequencies  [a]))

(defn función-frequencies-2
  [b]
  (frequencies [b]))

(función-frequencies-1 [1 2 2 5 6 0 1 2 3 3 4 9 8 6 5 5 4 3 33 2 2 2])
(función-frequencies-2 ['a' 'b' 'a' 'a'])
;Función Get
(defn función-get-1
  [a]
  (get [1 3 4 5 6 67 8 0 1 2] a))

(defn función-get-2
  [a b]
  (get [a] b))

(función-get-1 5)
(función-get-2 [10 20 30 40 50 60] 2)

;Función Get-In
(defn función-get-in-1
  [a b]
  (get-in b a))

(defn función-get-in-2
  [a c]
  (get-in a c))

(defn función-get-in-3
  [a b]
  (get-in a b))

(función-get-in-1 [[1 2 3]
                   [4 5 6]
                   [7 8 9]
                   [5 7 1]
                   [11 15 37]] [4])
(función-get-in-2 [[3 4 5 6 6]
                   [1 2 3 4 5]
                   ["a" "b" "c"]] [2])
(función-get-in-3 [[3 4 5 6 6]
                   [1 2 3 4 5]
                   [\a \b \c \d]
                   ["a" "b" "c"]
                   [true true false 1 4 5 "x" "z"]] [4 1])

;Función into
(defn función-into-1
  [a b]
  (into [b [a]]))

(defn función-into-2
  [a b]
  (into () [b [a]]))

(defn función-into-3
  [a b]
  (into #{} [[a] [b]]))

(función-into-1 [:g 5] [[:a 1] [:b 2] [:e 3]])
(función-into-2 [:g 5] [[:a 1] [:b 2] [:e 3]])
(función-into-3 [:a 1 :g 3 :f 6] [:b 9 :c 7 :d 4 :e 2])

;Función key
(defn función-key-1
  [a]
  (key a))
(defn función-key-2
  [a]
  (key (first a)))
(defn función-key-3
  [a]
  (key(first a)))

(función-key-1 (first {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6}))
(función-key-2 {:7 :j "s" 1 3 4})
(función-key-3 {:name "saul" 1 3 4 5})

;Función-keys
(defn función-Keys-1
  [a]
  (keys a))

(defn función-Keys-2
  [a]
  (keys a))

(función-Keys-1 {:uno 1 :dos 2 :tres 3 :cuatro 4 :cinco 5 :seis 6 :siete 7})
(función-Keys-2 {:a "a" :b "b" :c "c" :d "d" :e "e" :f "f" :g "g" :h "h" :i "i" :j "j" :k "k"})

;Función max
(defn función-max-1
  [a b]
  (max a b))

(defn función-max-2
  [a b c]
  (max a b c))

(defn función-max-3
  [a b c d e f g h]
  (max a b c d e f g h))

(función-max-1 30 30)
(función-max-2 30 35 60)
(función-max-3 100 200 400 200 500 600 400 150)

;Función merge
(defn función-Merge-1
  [a b]
  (merge a b))

(defn función-Merger-2
  [a]
  (merge a))

(defn función-Merger-3
  [a b c]
  (merge a b c))

(función-Merge-1 {:a 1 :b 2 :c 4} {:f 5 :b 9 :d 4})
(función-Merger-2 {:m 10 :l 20 :k 30 :j 50 :i 23 :h 45 :o 20})
(función-Merger-3 {:1 1 :2 2 :3 3 :4 4} {:5 "f" :6 "r" :7 100} {:8 6 :9 "d"})

;Función min
(defn función-min-1
  [a]
  (min a))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a]
  (min a))

(función-min-1 [10 20 40 302 02 10 30 20 301 2])
(función-min-2 20 30 10)
(función-min-3 [20 30 40 50])

;funcion neg?
(defn función-neg?-1
  [n]
  (neg? n))

(defn función-neg?-2
  [b a]
  (neg? (/ a b)))

(defn función-neg?-3
  [a b c]
  (neg? (- a b c)))

(función-neg?-1 -10)
(función-neg?-2 10 4)
(función-neg?-3 10 5 6)

;funcion nil?
(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [[a]]
  (nil? [a]))


(función-nil?-1 nil)
(función-nil?-2 [nil])

;fucion Max
(defn función-max-1
  [a b]
  (max a b))

(defn función-Max-2
  [a b c]
  (max a b c))

(defn función-Max-3
  [a b c d]
  (min (- a b) (+ b c)))

(función-max-1 12 34)
(función-Max-2 10 30 50)
(función-Max-3 340 220 39 180)

;Función not-empty
(defn función-not-empty-1
  [n]
  (not-empty n))

(defn función-not-empty-2
  [ a b c d]
  (not-empty [a b c d]))

(defn función-not-empty-3
  [n]
  (not-empty n))

(función-not-empty-1 [230 12])
(función-not-empty-2 10 20 30 40)
(función-not-empty-3 [])

;funcion nth
(defn función-nth-1
  [a b]
  (nth b a))

(función-nth-1 2 [10 20 30 40 50 60 70 80 90 100 200 300 400 50 600 700 800])
(función-nth-1 10 [10 20 true 40 50 false 60 70 80 "g" "x" "3" 300 400 50 600 700 800])
(función-nth-1 15[10 20 30 40 50 60 70 80 90 100 200 300 400 50 600 700 800])

;Función odd?
(defn función-odd?-1
  [n]
  (odd? n))

(defn función-odd?-2
  [[n]]
  (odd? (first [n])))

(defn función-odd?-3
  [a b]
  (odd? (- a b)))

(función-odd?-1 3)
(función-odd?-2 [20 30])
(función-odd?-3 5 2)

;Función range
(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))
(función-range-1 10)
(función-range-2 -10 10)
(función-range-3 -100 100 10)

;Función Partition
(defn función-partition-1
  [num]
  (partition num (range 20)))

(defn función-partition-2
  [a b]
  (partition a (range b)))

(defn función-partition-3
  [a b c]
  (partition a b (range c)))

(función-partition-1 4)
(función-partition-2 4 20)
(función-partition-3 4 6 20)

;Función partition-all
(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a c]
  (partition-all a c))

(defn función-partition-all-3
  [a b d]
  (partition-all a b d))

(función-partition-all-1 4 [0 1 2 33 55 3 4 5 6 7 8 9])
(función-partition-all-2 4 [0 false 1 2 "a" "b" "c" "d" 3 true 4 5 6 "s" 8 9])
(función-partition-all-3 2 4 [0 1 2 3 4 5 6 7 8 9])

;Función peek
(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))

(función-peek-1 [2 "a" \a 3 true "e" false 1])
(función-peek-2 [10 20 30 40 20 33 44 12 13 100 19])
(función-peek-3 '(1 4 2 11 3 22 5 10 6 7 8))

;funcion pop
(defn función-pop-1
  [x]
  (pop x))

(defn función-pop-2
  [a]
  (pop [a]))

(función-pop-1 [1 2 3 true 4 5 6 false "a"])
(función-pop-2 [10 20 30 40])

;Funciónn pos
(defn función-pos?-1
  [num]
  (pos? num))

(defn función-pos?-2
  [a b]
  (pos? (/ a b)))

(defn función-pos?-3
  [a b c]
  (pos? (- a (/ b c))))

(función-pos?-1 -2)
(función-pos?-2 10 15)
(función-pos?-3 10 8 7)

;Función qout
(defn función-qout-1
  [a]
  (quot 10 a))

(defn función-qout-2
  [a b]
(quot a b))

(defn función-qout-3
  [a b [c]]
  (quot (- a b) (first [c])))

(función-qout-1 3)
(función-qout-2 55 11)
(función-qout-3 5 3 [1 2 3 4 5])

;Función rem
(defn función-rem-1
  [a]
  (rem -10 a))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [[a] [b]]
  (rem (first [a]) (first[b])))

(función-rem-1 3)
(función-rem-2 10 -3)
(función-rem-3 [20 30 40] [20 15 2])

;Función repeat
(defn función-repeat-1
  [num]
  (repeat num "x"))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b [v]]
  (repeat (/ a b) (first [v])))

(función-repeat-1 5)
(función-repeat-2 16 "s")
(función-repeat-3 20 2 [11 222 333 44])

;Funciónrest
(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(función-rest-1 [10 20 30 40 50 60])
(función-rest-2 ["a" "x" "b" "c" "z" "d" "e" "y"])

;funcion select-keys
(defn función-select-keys-1
  [a b]
  (select-keys b a))

(defn función-select-keys-2
  [[a][b]]
  (select-keys [a] [b]) )

(defn función-select-keys-3
  [a b c d]
  (select-keys d [a b c]))

(función-select-keys-1 [:a] {:a 1 :b 2 :c 3 :d 4 :e 5})
(función-select-keys-2 [:a 1 :b 2 :c 3 :d 4 :e 5] [:a :b :c])
(función-select-keys-3 :d :h :g {:a 1 :b true :c 3 :d false :e 5 :f "a" :g "g" :h \a})

(defn función-sort-1
  [a]
  (sort a))

;Función sort
(defn función-sort-2
  [a]
  (sort [a]))

(función-sort-1 [2 5 6 9 0 6 0 3 4])
(función-sort-2 [2 5 6 9 10 16 17 20 40 10 45 6 0 3 4])

;funcion shuffle
(defn función-shuffle-1
  [a]
  (shuffle (list 1 2 4 a 6)))

(defn función-shuffle-2
  [a]
  (shuffle [a]))

(defn función-shuffle-3
  [n]
  (shuffle (range n)))

(función-shuffle-1 5)
(función-shuffle-2 [1 8 4 2 3])
(función-shuffle-3 10)

;Función Split-at
(defn función-split-1
  [a]
  (split-at a [1 2 3 4 5 6]))

(defn función-split-at-2
  [a [b]]
  (split-at a [b]))

(defn función-split-at-3
  [n]
  (split-at n [1 5 "f" true "l" false \a \b 6]))

(función-split-1 2)
(función-split-at-2 3 [1 2 3 4 5])
(función-split-at-3 4)

;Función subs
(defn función-subs-1
  [a]
  (subs "Hola Mundo!!!" a))

(defn función-subs-2
  [a b]
  (subs a b))

(defn función-subs-3
  [a b c]
  (subs a b c))

(función-subs-1 3)
(función-subs-2 "Tecnologico Nacional de Mexico" 10)
(función-subs-3 "oaxaca de juarez oax" 3 7)

;Función str
(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a b c]
  (str [a b c]))

(defn función-str-3
  [a b c]
  (str (+ a (/ b c))))

(función-str-1 123)
(función-str-2 4 5 6)
(función-str-3 10 8 4)

(defn función-subVec-1
  [a b]
  (subvec b a))

(defn función-subVec-2
  [a b c]
  (subvec c a b))

(defn función-subVec-3
  [n m f]
  (subvec f n m))

(función-subVec-1 4 [1 2 3 4 5 6 7 8])
(función-subVec-2 5 10 [1 2 4 5 6 7 8 9 0 11 1 2 13 1 5 16 1 7 15])
(función-subVec-3 8 15 ["a" "b" "c" true false \f \h "Ñ" "G" 4 7 1 \i "Y" "y" "j" 100 200 400 500 "3" "5" \o \g 20 30])

;Función take
(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a [b]]
  (take a [b]))

(defn función-take-3
  [a b c d e f g]
  (take g [a b c d e f]))

(función-take-1 4 '(10 20 30 50 60 200 404 303))
(función-take-2 3 [1 2 3 4 45 5])
(función-take-3 1 2 3 4 5 6 10)

;Función true?
(defn función-true?-1
  [n]
  (true? n))

(defn función-true?-2
  [a b]
  (true? (/ a b)))

(defn función-true?-3
  [n]
  (true? (first [range n])))

(función-true?-1 -10)
(función-true?-2 10 5)
(función-true?-3 -5)

;Función Val
(defn función-val-1
  [a]
  (val a))
(defn función-val-2
  [a]
  (val (first a)))
(defn función-val-3
  [a]
  (val (first a)))

(función-val-1 (first {:one :two :f :g 6 8}))
(función-val-2 {:3 :k :4 5})
(función-val-3 {"aaaa" "pppp" "gggg" "hhh" 1 4})

;Función vals
(defn función-vals-1
  [a b c d e f]
  (vals {:a a :b b :c c :d d :e e :f f}))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

(función-vals-1 1 2 3 4 5 6)
(función-vals-2 {:a 1 :b true :c 1 :d "f" :e 2 :f 3 :g "d" :h \h :i \a :j 10})
(función-vals-3 {:A 0 :B 1 :C 0 :1 1 :4 0 :D 0 :E 0 :F 1 :G 1 :H 1 :I 1 :J 0 :K 0})

;Función zero?
(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [b]
  (zero? (first (range b))))

(defn función-zero?-3
  [a b]
  (zero? (first (range a b))))

(función-zero?-1 0)
(función-zero?-2 5)
(función-zero?-3 -5 5)

;Función zipmap
(defn función-zipmap-1
  [a b c d t]
  (zipmap t [a b c d]))

(defn función-zipmap-2
  [[a] [b]]
  (zipmap [a] [b]))

(defn función-zipmap-3
  [n m]
  (zipmap m (repeat n)))

(función-zipmap-1 1 2 3 4 [:a :b :c :d])
(función-zipmap-2 [:a :b] [1 2])
(función-zipmap-3 3 [:a :b :c :d :f :1 :5 :g :h])


